/**
 * @file node_helper.js
 *
 * @author Karsten Hassel
 * @license MIT
 *
 * @see  https://gitlab.com/khassel/MMM-ModInstall
 */

const NodeHelper = require("node_helper");
const fs = require("node:fs");
const path = require("node:path");
const Log = require("logger");
const child_process = require("node:child_process");

/**
 * @module node_helper
 * @description Backend for the module.
 *
 * @requires external:node_helper
 */
module.exports = NodeHelper.create({
  name: "MMM-ModInstall",
  jsonList: null,
  stdio: "pipe", // for debugging you can set it to "inherit" to see stdout of execSync commands

  start() {
    Log.log(`Starting module helper: ${this.name}`);
  },

  fetchModuleList() {
    try {
      Log.log(`${this.name}: Fetching 3rd party module list.`);
      const url =
        "https://kristjanesperanto.github.io/MagicMirror-3rd-Party-Modules/data/modules.json";

      const response = child_process.execSync(
        `node -e "require('https').get('${url}', res => { let data = ''; res.on('data', chunk => { data += chunk; }); res.on('end', () => { process.stdout.write(data); }); }).on('error', err => { process.stderr.write(err.message); process.exit(1); });"`
      );
      const responseStr = response.toString("utf8");
      try {
        this.jsonList = JSON.parse(responseStr);
      } catch (parseError) {
        Log.error(
          `${this.name}: Failed to parse JSON response: ${parseError.message}`
        );
      }
      // Sort jsonList by lastCommit descending (this should ensure that for modules with same name the most recent is used) - this is untested.
      this.jsonList.sort(
        (a, b) => new Date(b.lastCommit) - new Date(a.lastCommit)
      );
      Log.log(`${this.name}: Found ${this.jsonList.length} 3rd party modules`);
    } catch (error) {
      Log.error(`${this.name}: Error fetching module list: ${error}`);
    }
  },

  installModules(modules) {
    let toInstall = [];
    for (const mod of modules) {
      let install = mod.install;
      if (!mod.install && mod.module && this.jsonList) {
        // search url in module list
        const jsonMod = this.jsonList.find(({ name }) => name === mod.module);
        if (jsonMod) install = { url: jsonMod.url };
      }
      if (install && install.url) {
        toInstall.push(install);
      }
    }

    Log.log(
      `${this.name}: Found ${toInstall.length} modules:\n\t- ${toInstall
        .map((instObj) => instObj.url.replace(/:\/\/.*@/g, "://"))
        .join("\n\t- ")}`
    );

    for (const install of toInstall) {
      let folder = install.url.replace(/.*\//g, "").replace(/\.git/gi, "");
      folder = path.resolve(`${__dirname}/../${folder}`);
      let maxtime = 300; // 5 min.
      if (install.timeout && install.timeout > 5) {
        try {
          maxtime = install.timeout;
        } catch (err) {
          Log.error(
            `${this.name}: Error setting timeout to ${install.timeout} ${err}, using default.`
          );
        }
      }
      if (!fs.existsSync(folder)) {
        try {
          Log.log(
            `${this.name}: Cloning missing module ${install.url} (max. ${maxtime} sec.)`
          );
          let cmd = `${install.url} ${folder}`;
          if (install.branch) cmd = `-b ${install.branch} ${cmd}`;
          child_process.execSync(`git clone ${cmd}`, {
            stdio: this.stdio,
            timeout: maxtime * 1000,
          });
        } catch (err) {
          Log.error(`${this.name}: Error cloning ${install.url} ${err}`);
        }
        const pck = `${folder}/package.json`;
        if (fs.existsSync(pck)) {
          try {
            const data = fs.readFileSync(pck, "utf8");
            if (data.includes('"dependencies":')) {
              cmd =
                "npm install --omit=dev --no-audit --no-fund --no-update-notifier";
              if (install.command) cmd = install.command;
              Log.log(
                `${this.name}: Installing module ${install.url} using ${cmd} (max. ${maxtime} sec.)`
              );
              child_process.execSync(`cd ${folder} && ${cmd}`, {
                stdio: this.stdio,
                timeout: maxtime * 1000,
              });
            }
          } catch (err) {
            Log.error(`${this.name}: Error installing ${install.url} ${err}`);
          }
        }
      }
    }
  },

  init() {
    Log.log(`${this.name}: Searching for missing modules ...`);

    const configFilename = path.resolve(
      global.configuration_file || `${global.root_path}/config/config.js`
    );

    let config;
    try {
      fs.accessSync(configFilename, fs.F_OK);
      const c = require(configFilename);
      config = Object.assign(c);
    } catch (e) {
      Log.error(`${this.name}: Couldn't read config.js`);
      return;
    }

    let autoInstall = false;
    const modSelf = config.modules.find(
      ({ module }) => module === `${this.name}`
    );
    if (modSelf && modSelf.config && modSelf.config.autoInstall) {
      autoInstall = true;
    }

    let modules = [];
    const addModule = (mod) => {
      const index = modules.findIndex((m) => m.module === mod.module);
      if (index < 0) {
        modules.push(mod);
      } else {
        if (!modules[index].install) modules[index] = mod;
      }
    };
    let needJsonList = false;
    for (const module of config.modules) {
      if (!modules.includes(module.module) && !module.disabled) {
        if (module.install) {
          addModule(module);
        } else if (autoInstall) {
          if (module.module !== `${this.name}`) {
            needJsonList = true;
            addModule(module);
          }
        }
      }
    }

    if (needJsonList) {
      this.fetchModuleList();
    }

    this.installModules(modules);
  },
});
