## Start Containers

```bash
cd /mnt/c/data/repo/k13/MMM-ModInstall

docker run --rm -it -u root --entrypoint bash -v $(pwd)/debug:/opt/magic_mirror/config -v $(pwd):/opt/magic_mirror/modules/MMM-ModInstall -p 8080:8080 karsten13/magicmirror:develop
```
