# MMM-ModInstall

This is a module for the [MagicMirror²-Project](https://github.com/MagicMirrorOrg/MagicMirror).

The standard installation procedure for each 3rd-party-module is always the same:

- clone the module into the `modules` folder
- if the new module needs additional dependencies run `npm install` in the `modules` folder
- add the new module to `config.js`

This module does the first 2 steps automatically while MagicMirror² is starting. You only have to install this module and add an additional `install` object to `config.js`.

## Installation

Assuming `~/MagicMirror` is the directory where you installed MagicMirror².

### Clone and install

```bash
cd ~/MagicMirror/modules
git clone https://gitlab.com/khassel/MMM-ModInstall.git
```

### Update your config.js file

Add a configuration block to the modules array in the `~/MagicMirror/config/config.js` file:

> ⚠️ The configuration block for `MMM-ModInstall` must be the first one in the modules list, otherwise the installation of modules placed before `MMM-ModInstall` is done to late (these modules will not work or need an update interval cycle to work)!

```js
let config = {
  modules: [
    {
      module: "MMM-ModInstall",
      config: {
        autoInstall: false
      },
    },
    ...
  ],
};
```

## Configuration

This module can clone and install a module if it knows the url for cloning.

### Using autoInstall

This is the lazy approach based on an idea by [Kristjan](https://github.com/KristjanESPERANTO). If you set `autoInstall: true` in the above config, the module fetches the 3rd party module list and tries to find the urls of all missing modules there.

Disadvantages:

- fetching 3rd party module list costs extra time (~20 sec.)
- there are some duplicate module names in the 3rd party module list so maybe the wrong one is cloned
- all modules are installed with the default install command `npm install --omit=dev --no-audit --no-fund --no-update-notifier` which will not work for all modules

### Using install objects

Without `autoInstall` every 3rd-party-module needs an additional `install` object with an `url` parameter in the `config.js`, e.g.

```js
let config = {
  modules: [
    {
      module: "MMM-Flights",
      install: {
        url: "https://gitlab.com/khassel/MMM-Flights.git",
        branch: "",
        command: "",
        timeout: 300; // in sec.
      },
      config: {
      },
    },
  ],
};
```

The parameter `branch` and `command` are optional, you can omit them if you not need them.

- `branch`: You can add a git ref here which will be checked out after cloning, if empty the default branch of the repository is used.
- `command`: You can specify an own install command, if empty `npm install --omit=dev --no-audit --no-fund --no-update-notifier` is used.
- `timeout`: You can specify an timeout in seconds for cloning and installation, if empty the default `300` (5 min.) is used.

With the `install` object in the `config.js` you have all information needed in this file. You can start from scratch (with this module installed and a modified `config.js` with `install` objects) and on the first start of MagicMirror² all (missing) modules are cloned and installed.

Adding a new module is simple, just add the module config into `config.js` including the `install` object and (re-)start MagicMirror².

### Mixed install

You can use both above install methods by setting `autoInstall: true` and providing `install` objects for some modules where you need this. If an `install` object is found this will be used, otherwise the autoInstall approach is used.
